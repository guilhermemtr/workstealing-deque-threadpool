#include <iostream>
#include <memory>

#include "examples/TaskLauncher.h"

#include "ThreadPool.h"
#include "ThreadWrapper.h"
#include "Task.h"
#include "AssertedTask.h"
#include "AssertedDataTask.h"
#include "DataTask.h"
#include "Future.h"

using namespace std;

int main(int argc, char ** argv)
{
    if(argc < 2) {
        cerr << "Usage : " << argv[0] << " <NumTests>" << endl;
        return -1;
    }

    int numTests = atoi(argv[1]);
    TaskLauncher tLaunch(numTests);
    if(argc == 3) {
        ThreadPool p(atoi(argv[2]), &tLaunch);
        Future * tlf = p.getFuture();
        tlf->wait();
        delete tlf;
    } else {
        ThreadPool p(&tLaunch);
        Future * tlf = p.getFuture();
        tlf->wait();
        delete tlf;
    }
    return 0;
}
