#ifndef TASK_H
#define TASK_H

#include <memory>

#include <assert.h>
#include "Future.h"
#include "ThreadPool.h"
#include "ThreadWrapper.h"

class ThreadPool;
class ThreadWrapper;
class DataTask;

#define execute(t) ((t).call(this->worker))

class Task
{
    friend class ThreadPool;
    friend class ThreadWrapper;
    friend class Future;

    public:

        /**
         * Default constructor.
         */
        inline Task(void) {
            this->future = new Future();
        }

        /**
         * Calls the task.
         */
        virtual inline void call(const ThreadWrapper * thr) {
            this->setWorker(thr);
            run();
            future->end();
        }

        /**
         * Runs the task.
         */
        virtual void run(void) = 0;

        /**
         * Submits a new task to the same scheduler in which the current task was scheduled to.
         * @param <code>Task *</code> t - The task so be submited.
         * @return <code>Future *</code> the future of that task.
         */
        virtual inline Future * submit(Task * t) {
            this->worker->sched->scheduleTask(this->worker->workerId, t);
            return t->getFuture();
        }

        /**
         * Submits a data task to the scheduler in which the current task was scheduled to.
         * @param <code>DataTask *</code> t - The task to be submited.
         * @return <code>Future *</code> the future of that task.
         */
        virtual Future * submit(DataTask * t);

        /**
         * Get's the future of the task.
         * @return <code>Future *</code> the future of the task.
         */
        inline Future * getFuture(void) {
            return this->future;
        }

        /**
         * Default destructor
         */
        virtual inline ~Task(void) {}

        //The future of the task.
        Future * future;

    protected:



        /**
         * Set's the worker which is going to run the task.
         * @param thr <code>const ThreadWrapper *</code> - the worker.
         */
        virtual inline void setWorker(const ThreadWrapper * thr) {
            this->worker = thr;
        }

        //The worker which is going to run the task.
        const ThreadWrapper * worker;
};

#endif // TASK_H
