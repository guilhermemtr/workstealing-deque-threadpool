#include <pthread.h>
#include <memory>
#include <vector>
#include "Task.h"

#ifndef __DEQUE_
#define __DEQUE_

#define __DEQUE_DATA_SIZE (1 << 23)

class Task;

class Queue {
  unsigned long int size;
  Task ** data;

  unsigned long int front;
  unsigned long int back;

  pthread_cond_t full_cond;
  pthread_cond_t empty_cond;

  pthread_mutex_t lock;

 public:
  Queue(unsigned long int size = __DEQUE_DATA_SIZE) {
    this->front = 0;
    this->back = 0;
    this->size = size;
    this->data = (Task **) malloc(sizeof(Task *) * size);
    pthread_mutex_init(&lock, NULL);
    pthread_cond_init(&full_cond, NULL);
    pthread_cond_init(&empty_cond, NULL);
  }

  virtual long int num_elems() {
    return back - front;
  }

  virtual long int get_size () {
    return size;
  }

  Task * tryDequeue() {
    pthread_mutex_lock(&lock);
    if(front == back) {
      pthread_mutex_unlock(&lock);
      return nullptr;
    }
    front--;
    Task * elem = data[front % size];
    pthread_mutex_unlock(&lock);
    return elem;
  }

  Task * trySteal() {
    pthread_mutex_lock(&lock);
    if(front == back) {
      pthread_mutex_unlock(&lock);
      return nullptr;
    }
    Task * elem = data[back % size];
    back++;
    pthread_mutex_unlock(&lock);
    return elem;
  }

  void enqueue(Task * elem) {
    pthread_mutex_lock(&lock);
    if(back - front == size) {
      pthread_mutex_unlock(&lock);
      throw "full queue";
    }
    data[front % size] = elem;
    front++;
    pthread_mutex_unlock(&lock);
  }

  void receive(Task * elem) {
    pthread_mutex_lock(&lock);
    if(front - back == size) {
      pthread_mutex_unlock(&lock);
      throw "full queue";
    }
    back--;
    data[back % size] = elem;
    pthread_mutex_unlock(&lock);
  }


  virtual ~Queue() {
    free(this->data);
  }
};

#endif
