#ifndef DATATASK_H
#define DATATASK_H

#include <cmath>
#include "Task.h"
#include "ThreadPool.h"

#define DATA_PARALLEL_NUM_PARTS 8
#define MAX_WORKERS (get_nprocs())

class DataTask : public Task
{
    friend class Task;
    public:

        /**
         * Default constructor.
         * @param parallelSize <code>long int</code> the parallel size of the data task.
         */
        DataTask(long int parallelSize):
            Task()
            {
                int maxWorkers = MAX_WORKERS;
                this->parallelSize = parallelSize;

                this->taskGroupingFactor = parallelSize / (DATA_PARALLEL_NUM_PARTS * maxWorkers);
                if (parallelSize < (DATA_PARALLEL_NUM_PARTS * maxWorkers)) {
                    this->taskGroupingFactor = 1;
                    this->numParallelParts = parallelSize;
                } else {
                    this->taskGroupingFactor = ((parallelSize + DATA_PARALLEL_NUM_PARTS*maxWorkers - 1)/(DATA_PARALLEL_NUM_PARTS*maxWorkers));
                    this->numParallelParts = (!(parallelSize%(DATA_PARALLEL_NUM_PARTS*maxWorkers))) ? parallelSize/taskGroupingFactor: 1 + parallelSize/taskGroupingFactor;
                }
                this->done = 0;
                this->current = 0;
            }

        /**
         * Calls the data task.
         */
        virtual inline void call(const ThreadWrapper * thr) {
            this->setWorker(thr);
            this->run();
        }

        /**
         * Prepares and runs the data task.
         */
        virtual inline void run(void) {
            long int base = __sync_fetch_and_add(&this->current,taskGroupingFactor);
            long int offset = taskGroupingFactor;
            if (offset > parallelSize - base) offset = parallelSize - base;
            this->run(base, offset);
            long int currentDone = __sync_add_and_fetch(&this->done,offset);

            /**
             * There might be a problem when a future is awaken, and then the task is deleted.
             * There could be still a worker running here.
             */

            if(currentDone == this->parallelSize) {
                this->future->end();
            }
        }

        /**
         * Runs the data task.
         * It's expected for the run function to compute from the base up to the base + offset.
         * @param base <code>long int</code> the computation base index.
         * @param offset <code>long int</code> the computation duration, or offset.
         */
        virtual void run(long int base, long int offset) = 0;

        /**
         * Default destructor
         */
        virtual ~DataTask(void) {}

    protected:

        //The current execution index.
        long int current;

        //The current completion index.
        long int done;

        //The data parallel size of the index.
        long int parallelSize;

        //The usual offset for a task.
        long int taskGroupingFactor;

        //Number of divisions of the data task.
        long int numParallelParts;

    private:
};

#endif // DATATASK_H
