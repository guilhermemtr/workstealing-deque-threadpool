#include <queue>

#include "RandomNumberGenerator.h"
#include "Task.h"
#include "Queue.h"
#include "../utils/WorkGroup.h"
#include "../utils/QueueData.h"

class Scheduler;

#ifndef SCHEDULER_H
#define SCHEDULER_H

class Task;

class Scheduler
{
    public:

        /**
         * Default constructor
         */
        inline Scheduler(void) {
            this->numQueues = get_nprocs();
            this->queues = new Queue[this->numQueues];
            this->queueStats = new QueueData[this->numQueues];
        }

        /**
         * Schedules a task to the scheduler.
         * @param index <code>long int</code> the workers index.
         * The index depends on the sheduled implemented.
         * @param t <code>Task *</code> the task to be scheduled.
         */
        virtual inline void scheduleTask(int index, Task * t) {
            this->queues[index].enqueue(t);
        }

        /**
         * Tries to get a scheduled task.
         * @param index <code>long int</code> the workers index.
         * @param prng <code>RandomNumberGenerator *</code> the random number generator.
         * @return <code>Task *</code> the retrieved task from the scheduler.
         * May return <code>nullptr</code> if currently there aren't any tasks.
         */

        virtual inline Task * getScheduledTask(const unsigned int index, RandomNumberGenerator * prng) {
            Queue * q = this->queues;
            Task * t = q[index].tryDequeue();
            if(t != nullptr) {
                return t;
            }
            const unsigned int nQueues = this->numQueues;
            int tries = (nQueues << 10) - 1;
            unsigned int seed;
            seed = prng->frandom();
            seed %= this->numQueues;
            while(tries > 0)
            {
                if(seed == index)
                    seed = seed + 1 >= nQueues ? 0 : seed + 1;
                t = q[seed].trySteal();
                if(t != nullptr) return t;
                seed = seed + 1 >= nQueues ? 0 : seed + 1;
                tries--;
            }
            return t;
        }

        /**
         * Default destructor
         */
        virtual ~Scheduler(void) {
            delete [] this->queueStats;
            delete [] this->queues;
        }

        //The queues.
        Queue * queues;

        //The number of queue.
        unsigned int numQueues;

    private:

        //The queue usage statistics
        QueueData * queueStats;

        //The work groups
        Workgroup groupings;

};

#endif // SCHEDULER_H
