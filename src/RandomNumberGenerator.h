#include <stdlib.h>

#ifndef RANDOMNUMBERGENERATOR_H
#define RANDOMNUMBERGENERATOR_H

class RandomNumberGenerator
{
    public:

        /**
         * Default constructor
         */
        inline RandomNumberGenerator() {
            m_z = random();
            m_w = random();
        }

        /**
         * Gets a random number.
         * @return <code>long int</code> a random number generated.
         */
        inline unsigned int frandom() {
            m_z = 36969 * (m_z & 65535) + (m_z >> 16);
            m_w = 18000 * (m_w & 65535) + (m_w >> 16);
            return (m_z << 16) + m_w;
        }

        /**
         * Default destructor
         */
        virtual inline ~RandomNumberGenerator() {}

        int m_z;
        int m_w;
};

#endif // RANDOMNUMBERGENERATOR_H
