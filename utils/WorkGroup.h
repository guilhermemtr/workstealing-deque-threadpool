#include <iostream>
#include <istream>

#include "CpuInfo.h"

#ifndef __GROUPS__
#define __GROUPS__

class Workgroup {
public:
    Workgroup();
    ~Workgroup();
    std::string toString();

    //total number of groups
    long num_groups;
    //number of levels of groups.
    long num_group_levels;
    //total number of workers
    long num_workers;
    //group size by level.
    long * group_size;
    //num groups per level.
    long * num_level_groups;
    //given the level and the cpu, get's the group id.
    long ** group_id;
    //queue_t [level][cpu_id][QUEUES_OF_A_GROUP] queue_indexes;
    long *** group_queues;
    //the size to hierarchy_level conversor.
    long * correct_level_index;
    //the number of distinct levels.
    long num_distinct_levels;
    //the distinct levels ordered.
    long * distinct_levels;
};

/**
 * Gets the number of queues.
 * @return <code>number</code> number of cpus.
 */
#define get_numqueues (get_numcpus())

/**
 * Gets the queues for a given cpu, on a certain level.
 * @param groupings <code>worker_group_t</code> the groups.
 * @param level <code>long</level> the level.
 * @param cpu <code>int</code> the cpu.
 * @return <code>long *</code> the queues.
 */
#define get_queues(groupings,level,cpu) (groupings.group_queues[level][cpu])

/**
 * Gets a certain group size.
 * @param groupings <code>worker_group_t</code> the groups.
 * @param level <code>long</code> the level.
 * @return <code>long</code> the group size.
 */
#define get_group_size(groupings,level) (groupings.group_size[level])

/**
 * Gets the number of distinct levels.
 * @param groupings <code>worker_group_t</code> the groups.
 * @return <code>long</code> the number of distinct levels.
 */
#define get_num_distinct_levels(groupings) (groupings.num_distinct_levels)

/**
 * Gets the distinct levels.
 * @param groupings <code>worker_group_t</code> the groups.
 * @return the distinct levels.
 */
#define get_distinct_levels(groupings) (groupings.distinct_levels)

/**
 * Gets the best fitted size of a certain level for a given size.
 * @param groupings <code>worker_group_t</code> the groups.
 * @param size <code>long</code> the group size.
 * @return the best fitted level number.
 */
#define get_best_fitted_level(groupings,size)				\
  (									                        \
   size >= groupings.num_workers ?				        	\
   groupings.num_group_levels -1:				        	\
   groupings.correct_level_index[size]			    		\
  )

#endif
