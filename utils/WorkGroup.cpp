#include "WorkGroup.h"

Workgroup::Workgroup()
{
    CpuInfo * cpuInfo = new CpuInfo();
    this->num_group_levels = cpuInfo->getNumCaches() + 2;
    this->num_workers = get_numcpus();
    this->num_groups = this->num_workers;

    this->group_size = (long int *) malloc(this->num_group_levels*sizeof(long int));
    this->num_level_groups = (long int *) malloc(this->num_group_levels*sizeof(long int));

    this->group_size[0] = 1;
    this->num_level_groups[0] = this->num_workers;

    this->group_size[this->num_group_levels-1] = this->num_workers;
    this->num_level_groups[this->num_group_levels-1] = 1;

    for(long int i = 1; i < this->num_group_levels - 1; i ++) {
        this->group_size[i] = this->num_workers / cpuInfo->getNumGroups(i-1);
        this->num_level_groups[i] = cpuInfo->getNumGroups(i-1);
        this->num_groups += cpuInfo->getNumGroups(i-1);
    }

    this->group_id = (long int **) malloc(this->num_group_levels * sizeof(long int *));
    for(long int i = 0; i < this->num_group_levels; i ++) {
        this->group_id[i] = (long int *) malloc(this->num_workers * sizeof(long int));
        for(long j = 0; j < this->num_workers; j ++) {
            if(i == 0) {
                this->group_id[i][j] = j;
            } else if (i == this->num_group_levels - 1) {
                this->group_id[i][j] = 0;
            } else {
                this->group_id[i][j] = cpuInfo->getGroup(j,i-1);
            }
        }
    }

    long int *** group_queues =  (long int ***) malloc(this->num_group_levels * sizeof(long int ***));
    this->group_queues = group_queues;
    //sets up the levels
    for(long int i = 0; i < this->num_group_levels; i ++) {
        //sets up the groups
        group_queues[i] = (long int **) malloc( this->num_workers * sizeof(long int *));
        for(long int j = 0; j < this->num_workers; j ++) {
            //sets up the indexes belonging to each queue.
            group_queues[i][j] =  (long int *) malloc(this->group_size[i] * sizeof(long int));
            long int group_id = this->group_id[i][j];
            long int curr = 0;
            for(long int k = 0; k < this->group_size[i]; k++) {
                while(this->group_id[i][curr] != group_id) curr++;
                group_queues[i][j][k] = curr;
                curr++;
            }
        }
    }
    this->correct_level_index = (long int *) calloc(this->num_workers, sizeof(long int));
    long current_level_index = 0;
    long current_level_size = 1;
    long distinct_levels = 1;
    for(long int i = 0; i < this->num_workers; i++) {
        if(i > current_level_size) {
            distinct_levels++;
            long int j = current_level_index;
            while(this->group_size[j] < i) j++;
            current_level_size = this->group_size[j];
            current_level_index = j;
        }
        this->correct_level_index[i] = current_level_index;
    }
    this->num_distinct_levels = distinct_levels;
    this->distinct_levels = (long int *) calloc(this->num_distinct_levels,sizeof(long int));
    current_level_index = 0;
    long int j = 0;
    for(long int i = 0; i < this->num_workers; i++) {
        if(this->correct_level_index[i] != current_level_index) {
            this->distinct_levels[j++] = current_level_index;
            current_level_index = this->correct_level_index[i];
        }
    }
    this->distinct_levels[j] = current_level_index;
    delete cpuInfo;
}

Workgroup::~Workgroup()
{
    long int *** group_queues = this->group_queues;
    for(long int i = 0; i < this->num_group_levels; i ++) {
        for(long int j = 0; j < this->num_workers; j ++) {
            free(group_queues[i][j]);
        }
        free(group_queues[i]);
    }
    free(group_queues);
    for(long int i = 0; i < this->num_group_levels; i ++) {
        free(this->group_id[i]);
    }
    free(this->correct_level_index);
    free(this->distinct_levels);
    free(this->group_id);
    free(this->group_size);
    free(this->num_level_groups);
}

std::string Workgroup::toString()
{
    std::string str = "";
    str.append("Group levels: ").append(std::to_string(num_group_levels));
    str.append("\nNumber of groups: ").append(std::to_string(num_groups));
    str.append("\nNumber of workers: ").append(std::to_string(num_workers));
    str.append("\nGroup sizes: \n");
    for(long int i = 0;i < num_group_levels; i ++) {
        str.append("Group of level ").append(std::to_string(i));
        str.append(" has size of ").append(std::to_string(group_size[i]));
        str.append("\nLevel ").append(std::to_string(i));
        str.append(" has ").append(std::to_string(num_level_groups[i]));
        str.append(" groups\n");
    }
    for(long int i = 0; i < num_group_levels; i ++) {
        str.append("Groups for level ").append(std::to_string(i)).append("\n");
        for(long int j = 0; j < num_workers; j ++) {
            str.append("Level ").append(std::to_string(i)).append("\nCPU ").append(std::to_string(j)).append(":\t").append(std::to_string(group_id[i][j])).append("\n");
        }
    }
    str.append("Correct hierarchy level from each size:\n");
    for(long int i = 0; i < num_workers; i ++) {
        str.append("For size ").append(std::to_string(i)).append(" the best fitted group is ").append(std::to_string(correct_level_index[i])).append("\n");
    }
    str.append("Distinct levels of the hierarchy:\n");
    for(long int i = 0; i < num_distinct_levels; i ++) {
        str.append("Level ").append(std::to_string(distinct_levels[i])).append(" has size of ").append(std::to_string(group_size[distinct_levels[i]])).append("\n");
    }
    return str;
}

