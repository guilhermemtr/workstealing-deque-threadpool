#include <sys/sysinfo.h>
#include <sched.h>
#include <fstream>
#include <string>
#include "StringUtils.h"

#ifndef __CPUINFO__
#define __CPUINFO__

#define NUM_CACHES 4

class CpuInfo {
public:
    CpuInfo();

    ~CpuInfo() {
        delete simblings;
        delete groupCounters;
    }

    int getNumCaches() {
        return NUM_CACHES;
    }

    int getNumGroups(int level) {
        return (*groupCounters)[level];
    }

    std::string toString();

    int getGroup(int cpu, int cache_nr) {
        return (*simblings)[this->numSimblings * cpu + cache_nr];
    }

    std::vector<int> * getSimblings() {
        return simblings;
    }

    int getNumCpus() {
        return numCpus;
    }

    int getNumSimblings() {
        return numSimblings;
    }

private:
    void initCpuInfo();

    //the number of available cpus.
    int numCpus;

    //the number of levels of cache.
    int numSimblings;

    //4096 maximum cpus linux kernel can handle
    std::vector<int> * simblings;

    //the group counters
    std::vector<int> * groupCounters;
};

/**
 * Gets the number of cpus.
 * @return <code>int</code> the number of cpus configured for the operating system.
 */
#define get_numcpus() (get_nprocs_conf())

/**
 * Gets the current cpu.
 * @return <code>int</code> the current cpu on which the calling thread is running.
 */
#define get_cpu() (sched_getcpu())

#endif /* __CPUINFO__ */
