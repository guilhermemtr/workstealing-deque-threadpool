#ifndef TIMELIB_H
#define TIMELIB_H

#include <chrono>

class TimeLib
{
    public:
        /** Default constructor */
        TimeLib();

        void start();

        void end();

        float getDuration();

        /** Default destructor */
        virtual ~TimeLib();
    protected:
    private:
        std::chrono::system_clock::time_point startTime;
        std::chrono::system_clock::time_point endTime;
};

#endif // TIMELIB_H
